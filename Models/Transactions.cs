﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Transactions
    {
        public int? TransactionsId { get; set; }
        public int? EmployeesId { get; set; }
        public Employees? Employees { get; set; }
        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }
        [Required]
        public string? Name { get; set; }

    }
}
