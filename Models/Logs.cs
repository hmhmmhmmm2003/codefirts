﻿namespace KiemTra.Models
{
    public class Logs
    {
        public int LogsId { get; set; }
        public int TransactionalId {  get; set; }
        public DateTime Logindate { get; set; }
        public DateTime LoginTime { get; set; }
        public Transactions? Transactions { get; set; }

    }
}
