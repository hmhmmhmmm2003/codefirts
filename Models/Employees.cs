﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Employees
    {
         public int EmployeesId { get; set; }
        [Required]
        public string? FirstName {  get; set; }
        [Required]
        public string? LastName { get; set; }
        public string? Contactandaddress { get; set; }
        public string? Username {  get; set; }
        public string? Password {  get; set; }
    }
}
