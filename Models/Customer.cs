﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        [Required]
        public string? FirstName { get; set; }
        [Required]
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }

    }
}
