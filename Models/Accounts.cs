﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Accounts
    {
        [Required]
        public int AccountsId { get; set; }
        public Customer? Customer { get; set; }
        [Required]
        public string? AccountsName { get; set; }
       
    }
}
