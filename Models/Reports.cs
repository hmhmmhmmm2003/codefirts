﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Reports
    {
        public int ReportsId { get; set; }
        public int AccountsId { get; set; }
        public Accounts? Accounts { get; set; }
        public int LogsID { get; set; }
        public Logs? Logs { get; set; }
        public int TransactionsId {  get; set; }
        public Transactions? Transactions { get; set; }
        public string? ReportName { get; set; }
        public DateTime ReportDate { get; set; }
    }
}
