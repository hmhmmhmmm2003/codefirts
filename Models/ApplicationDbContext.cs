﻿using Microsoft.EntityFrameworkCore;

namespace KiemTra.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>options) : base(options){ }
        public DbSet<Accounts> accounts { get; set; }
        public DbSet<Customer> customers { get; set; }
        public DbSet<Employees> employees { get; set; }
        public DbSet<Logs> logs { get; set; }
        public DbSet<Reports> reports { get; set; }
        public DbSet<Transactions> transactions { get; set; }

    }
}
