﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KiemTra.Migrations
{
    /// <inheritdoc />
    public partial class InitialCrate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "customers",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customers", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "employees",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Contactandaddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employees", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "accounts",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerID = table.Column<int>(type: "int", nullable: true),
                    AccountsName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_accounts_customers_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "customers",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "transactions",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    EmployeesID = table.Column<int>(type: "int", nullable: true),
                    CustomerID = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transactions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_transactions_customers_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "customers",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_transactions_employees_EmployeesID",
                        column: x => x.EmployeesID,
                        principalTable: "employees",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "logs",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionalID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Logindate = table.Column<DateOnly>(type: "date", nullable: false),
                    LoginTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TransactionsID = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_logs_transactions_TransactionsID",
                        column: x => x.TransactionsID,
                        principalTable: "transactions",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "reports",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountsID = table.Column<int>(type: "int", nullable: true),
                    LogsID = table.Column<int>(type: "int", nullable: false),
                    TransactionsID = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    ReportName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ReportDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reports", x => x.ID);
                    table.ForeignKey(
                        name: "FK_reports_accounts_AccountsID",
                        column: x => x.AccountsID,
                        principalTable: "accounts",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_reports_logs_LogsID",
                        column: x => x.LogsID,
                        principalTable: "logs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_reports_transactions_TransactionsID",
                        column: x => x.TransactionsID,
                        principalTable: "transactions",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_accounts_CustomerID",
                table: "accounts",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_logs_TransactionsID",
                table: "logs",
                column: "TransactionsID");

            migrationBuilder.CreateIndex(
                name: "IX_reports_AccountsID",
                table: "reports",
                column: "AccountsID");

            migrationBuilder.CreateIndex(
                name: "IX_reports_LogsID",
                table: "reports",
                column: "LogsID");

            migrationBuilder.CreateIndex(
                name: "IX_reports_TransactionsID",
                table: "reports",
                column: "TransactionsID");

            migrationBuilder.CreateIndex(
                name: "IX_transactions_CustomerID",
                table: "transactions",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_transactions_EmployeesID",
                table: "transactions",
                column: "EmployeesID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "reports");

            migrationBuilder.DropTable(
                name: "accounts");

            migrationBuilder.DropTable(
                name: "logs");

            migrationBuilder.DropTable(
                name: "transactions");

            migrationBuilder.DropTable(
                name: "customers");

            migrationBuilder.DropTable(
                name: "employees");
        }
    }
}
